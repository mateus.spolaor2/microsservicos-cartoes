package br.com.pagamentos.pagamento.models;

import br.com.pagamentos.pagamento.DTOs.PagamentoResponseDTO;

import javax.persistence.*;

@Entity
@Table(name = "pagamentos")
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int idCartao;

    private String descricao;

    private Double valor;

    public Pagamento(){}

    public Pagamento(int id, int idCartao, String descricao, double valor) {
        this.id = id;
        this.idCartao = idCartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public PagamentoResponseDTO toDTO(){
        PagamentoResponseDTO responseDTO = new PagamentoResponseDTO();

        responseDTO.setId(this.id);
        responseDTO.setDescricao(this.descricao);
        responseDTO.setIdCartao(this.idCartao);
        responseDTO.setValor(this.valor);

        return responseDTO;
    }
}
