package br.com.pagamentos.pagamento.clients;

import br.com.pagamentos.pagamento.clients.decoder.CartaoClientConfiguration;
import br.com.pagamentos.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/info/{idCartao}")
    Cartao pesquisaPorId(@PathVariable int idCartao);

}
