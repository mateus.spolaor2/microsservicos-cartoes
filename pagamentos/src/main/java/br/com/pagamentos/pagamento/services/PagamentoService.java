package br.com.pagamentos.pagamento.services;

import br.com.pagamentos.pagamento.DTOs.PagamentoResponseDTO;
import br.com.pagamentos.pagamento.DTOs.RealizaPagamentoRequestDTO;
import br.com.pagamentos.pagamento.clients.CartaoClient;
import br.com.pagamentos.pagamento.clients.PagarFaturaResponse;
import br.com.pagamentos.pagamento.models.Cartao;
import br.com.pagamentos.pagamento.models.Pagamento;
import br.com.pagamentos.pagamento.repositories.PagamentoRepository;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    CartaoClient cartaoClient;

    @Autowired
    PagamentoRepository pagamentoRepository;

    public PagamentoResponseDTO realizaPagamento(RealizaPagamentoRequestDTO pagamentoDTO) {
        Cartao cartaoUtilizado = cartaoClient.pesquisaPorId(pagamentoDTO.getIdCartao());

        if(cartaoUtilizado.isAtivo()){
            Pagamento novoPagamento = new Pagamento();
            novoPagamento.setIdCartao(cartaoUtilizado.getId());
            novoPagamento.setDescricao(pagamentoDTO.getDescricao());
            novoPagamento.setValor(pagamentoDTO.getValor());

            Pagamento pagamentoResponse = pagamentoRepository.save(novoPagamento);
            return pagamentoResponse.toDTO();
        } else {
            throw new RuntimeException("O cartão " + cartaoUtilizado.getNumero()
                    + "/ID: "+ cartaoUtilizado.getId() + " não está ativo");
        }

    }


    public List<PagamentoResponseDTO> buscaPagamentosPorCartao(int idCartao) {

        Cartao cartaoDB = cartaoClient.pesquisaPorId(idCartao);
        List<PagamentoResponseDTO> pagamentosDoCartao = new ArrayList<>();

        for (Pagamento pagamentoDB : pagamentoRepository.findAllByIdCartao(cartaoDB.getId())) {
            pagamentosDoCartao.add(pagamentoDB.toDTO());
        }

        return pagamentosDoCartao;
    }

    public PagarFaturaResponse pagaFatura(int cartaoId) {
        int valorTotal = 0;
        LocalDate dataAtual = LocalDate.now();

        for (PagamentoResponseDTO pagamento : buscaPagamentosPorCartao(cartaoId)){
            valorTotal += pagamento.getValor();
        }

        pagamentoRepository.deleteByIdCartao(cartaoId);

        PagarFaturaResponse faturaPaga = new PagarFaturaResponse(cartaoId, valorTotal, dataAtual);
        return faturaPaga;
    }
}
