package br.com.cartoes.cartao.repositories;

import org.springframework.data.repository.CrudRepository;
import br.com.cartoes.cartao.models.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

    Cartao findByNumero(String numeroCartao);

    boolean existsByNumero(String numeroCartao);
}
