package br.com.faturas.fatura.services;

import br.com.faturas.fatura.clients.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FaturaService {

    @Autowired
    PagamentoClient pagamentoClient;

    @Autowired
    CartaoClient cartaoClient;

    public List<Pagamento> exibeFatura(int clienteId, int cartaoId) {
        if(checaProprietarioDoCartao(clienteId, cartaoId)){
            List<Pagamento> pagamentosPorCartao =
                    pagamentoClient.buscaPagamentosPorCartao(cartaoId);

            return pagamentosPorCartao;

        } else {
            throw new RuntimeException("Cartão da fatura solicitada não é atrelado à este cliente");
        }
    }

    public FaturaPagaResponse pagarFatura(int clienteId, int cartaoId) {
        if(checaProprietarioDoCartao(clienteId, cartaoId)){
            FaturaPagaResponse faturaPaga = pagamentoClient.pagarFatura(cartaoId);
            return faturaPaga;
        } else {
            throw new RuntimeException("Cartão da fatura solicitada não é atrelado à este cliente");
        }
    }

    private boolean checaProprietarioDoCartao(int clienteId, int cartaoId){
        Cartao cartaoDb = cartaoClient.pesquisaPorId(cartaoId);
        if(clienteId == cartaoDb.getClienteId()){
            return true;
        } else {
            return false;
        }
    }

}
