package br.com.faturas.fatura.clients.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND,
        reason = "Cartão ou cliente não constam na base de dados")
public class RecursoNotFoundException extends RuntimeException {
}

