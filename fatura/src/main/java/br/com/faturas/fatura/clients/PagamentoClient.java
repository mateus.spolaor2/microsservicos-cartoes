package br.com.faturas.fatura.clients;

import br.com.faturas.fatura.clients.decoders.pagamento.PagamentoClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = "PAGAMENTO", configuration = PagamentoClientConfiguration.class)
public interface PagamentoClient {

    @GetMapping("/pagamentos/{idCartao}")
    List<Pagamento> buscaPagamentosPorCartao(
            @PathVariable(name = "idCartao") int idCartao);

    @PostMapping("/pagamentos/{idCartao}/pagar")
    FaturaPagaResponse pagarFatura(
            @PathVariable(name = "idCartao")int cartaoId);
}
