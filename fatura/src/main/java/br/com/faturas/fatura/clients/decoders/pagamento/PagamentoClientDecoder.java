package br.com.faturas.fatura.clients.decoders.pagamento;

import br.com.faturas.fatura.clients.exceptions.CartaoNotFoundException;
import br.com.faturas.fatura.clients.exceptions.RecursoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PagamentoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new RecursoNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }

}
