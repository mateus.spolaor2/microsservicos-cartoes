package br.com.faturas.fatura.controllers;

import br.com.faturas.fatura.clients.FaturaPagaResponse;
import br.com.faturas.fatura.clients.Pagamento;
import br.com.faturas.fatura.services.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


/*
POST /fatura/{cliente-id}/{cartao-id}/pagar
Paga a fatura de um cartão do cliente (apaga os registros do banco de dados)

Response 200
{
    "id": 1,
    "valorPago": 21.0,
    "pagoEm": "2020-03-11",
}
 */
@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    FaturaService faturaService;

    @GetMapping("/{clienteId}/{cartaoId}")
    public List<Pagamento> exibeFatura (
            @PathVariable(name = "clienteId") int clienteId,
            @PathVariable(name = "cartaoId") int cartaoId){
        try {
            return faturaService.exibeFatura(clienteId, cartaoId);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{clienteId}/{cartaoId}/pagar")
    public FaturaPagaResponse pagarFatura(
            @PathVariable(name = "clienteId") int clienteId,
            @PathVariable(name = "cartaoId") int cartaoId){
        try{
            return faturaService.pagarFatura(clienteId, cartaoId);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
